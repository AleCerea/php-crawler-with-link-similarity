<?php 

	findAndCompare();	

	// Main Function
	function findAndCompare()
	{
		if(!isset($_POST) || empty($_POST))
		{
			return header("Location:/");
		}

		if(url_exists($_POST['first_site']))
		{
			$link_1 = $_POST['first_site'];
		}
    	else
    	{?>
    		<p>The url <?php echo $_POST['first_site']; ?> does not exists! Please check it!</p>
    		<a href="/">Back to Form</a>			
		<?php 
			return;
    	}

		if(url_exists($_POST['second_site']))
		{
			$link_2 = $_POST['second_site'];
		}
    	else
    	{?>
    		<p>The url <?php echo $_POST['second_site']; ?> does not exists! Please check it!</p>
    		<a href="/">Back to Form</a>			
		<?php 
			return;
    	}
		
		// Import Html data
		$code_link_1 = import_html_data($link_1);

		// Import Html data
		$code_link_2 = import_html_data($link_2);

		// create similarity links array
		
		$anchor_links_1 = extract_html_element($code_link_1, $link_1);

		$anchor_links_2 = extract_html_element($code_link_2, $link_2);

		print_csv(get_links_similarity_array($anchor_links_1,$anchor_links_2));
	}

	function print_csv($data = [], $columns_header = ["First Site", "Second Site", "Similarity"], $filename = "Similarity_report.csv")
	{
		// open handler 
		$fp = fopen('php://output', 'w');

		// set header
		header('Content-type: application/csv');
		header('Content-Disposition: attachment; filename='.$filename);

		// set columns header
		fputcsv($fp, $columns_header);

		// set data
		foreach ($data as $key => $value) {
			fputcsv($fp, $value);
		}

		// close
		fclose($fp);
	}

	// Import HTML data in CURL (timeout 400)
	function import_html_data($siteUrl = null)
	{
		if ($siteUrl === null) return;

		// init curl	
		$ch =  curl_init();
		//return the transfer as a string 
	    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

	    // set url 
		curl_setopt($ch, CURLOPT_URL, $siteUrl);
		curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 0); 
		curl_setopt($ch, CURLOPT_TIMEOUT, 400); //timeout in seconds

		// get HTML code
  		$html_code = curl_exec($ch);

  		curl_close($ch);

  		return $html_code;
	}

	// Parse DOM elements and estract an array of links (a href)
	function extract_html_element($data = null, $site_url = null)
	{
		if (($data == null) || ($site_url == null)) return;

		# Create a DOM parser object
		$dom = new DOMDocument();

		#parse
		@$dom->loadHTML($data);

		$site_1_anchors = [];

		foreach($dom->getElementsByTagName('a') as $link) {

	    	#get the link
			$single_link = $link->getAttribute('href');
			if ($debugmode) 	
				echo "Single Link: ".$single_link."<br />";
			

			#parse link to get path
			#$single_link_path = parse_url($single_link, PHP_URL_PATH);		
			#if ($debugmode) 
			#	echo "Parsed URL: ".$single_link_path."<br />";
			

			#check if same domain
			if ( (parse_url($single_link, PHP_URL_HOST) == parse_url($site_url, PHP_URL_HOST)) )
			{
				if ($debugmode) 
						echo "SAME DOMAIN ".$single_link." - ".$site_url."<br />";

				#check if link is already inserted in array
				if ( array_search($single_link, $site_1_anchors) === false)  
				{
					if ($debugmode) 
						echo "Inserted link: ".$single_link."<br />";
					array_push($site_1_anchors, $single_link);
				}else
					if ($debugmode) 
						echo "Link alredy in array: ".$single_link."<br />";
			}
			else
				if ($debugmode) 
						echo "OTHER DOMAIN"."<br />";;
		}

		return $site_1_anchors;
	}

	// Match links
	function get_links_similarity_array($link_1 = [], $link_2 = [])
	{

		if (($link_1 === null) || ($link_2 === null) ) return;


		$output = [];

		# Check links similarity
		foreach ($link_1 as $value1) {
			
			#initialization
			$similarity = 0;
			$compare_term = "";


			foreach ($link_2 as $value2) {
				similar_text(parse_url($value1, PHP_URL_PATH), parse_url($value2, PHP_URL_PATH),$affinity);
				if ($affinity > $similarity){
					$compare_term = $value2;
					$similarity = $affinity;
				}			

			}
			if ($debugmode) 
				echo $value1 . "<br />" . $compare_term . "<br />SIMILARITY = ".round($similarity, 2, PHP_ROUND_HALF_UP)." % <br/>";
			array_push($output, [$value1,$compare_term,round($similarity, 2, PHP_ROUND_HALF_UP)]);

		}

		return $output;
	}

	// Check if url exists (code 200)
	function url_exists($url){
    	$ch = curl_init($url);
	    curl_setopt($ch, CURLOPT_HEADER, TRUE);
	    curl_setopt($ch, CURLOPT_NOBODY, TRUE);
	    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, FALSE);
	    curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
    	$status = [];
    	preg_match('/HTTP\/.* ([0-9]+) .*/', curl_exec($ch) , $status);
    	 return ($status[1] == 200);    	
	}

?>