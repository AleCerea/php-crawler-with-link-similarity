<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<!-- Latest compiled and minified CSS -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

	<!-- Optional theme -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">

	<script
  src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
  integrity="sha256-3edrmyuQ0w65f8gfBsqowzjJe2iM6n0nKciPUp8y+7E="
  crossorigin="anonymous"></script>

	<!-- Latest compiled and minified JavaScript -->
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>

	<title>Test</title>
</head>
<body>
	<div class="container">
		<h1>Test</h1>
		<h4><em>Insert valid site url to get a csv with internal links similarity </em></h4>
		<form action="/functions.php" method="POST">
			<div class="form-group">
				<label for="first_site" class="field">First Site Url <span id="char_position_count" style="display: none"></span></label>
				<input type="text" name="first_site" id="first_site_input"  class="form-control" placeholder="example : https://www.gazzetta.it" required>
			</div>
			<div class="form-group">
				<label for="second_site">Second Site Url</label>
				<input type="text" name="second_site" id="second_site_input"  class="form-control" placeholder="example : https://www.corriere.it" required>
			</div>
			<input type="submit" value="Submit"  class="btn btn-primary">
			</fieldset>
		</form>
		<div id="counter" class="text-center" style="display: none;">
			<label for="">You have inserted <span id="char_count"></span> letters </label>
		</div>
	</div>
	<script>
		$(document).ready(function(){
			$('.form-control').change(function(){
				// Bonus 1
				let counter = $('#first_site_input').val().length + $('#second_site_input').val().length;
				$('#char_count').text( counter );
				$('#counter').toggle(counter > 0);

				// Bonus 2

				let first_input_value = $("#first_site_input").val();
				counter = 0;
				for (var i = 0; i < first_input_value.length; i++) {
					var code = first_input_value.toUpperCase().charCodeAt(i)
					if (code > 64 && code < 91)
						code = (code - 64);
					counter = counter + code;
				}
				$('#char_position_count').text( "[Sum letter's alphabetical positions : "+counter +"]");
				$('#char_position_count').toggle(counter > 0);			
			})			
		});
	</script>
</body>
</html>